# Minimal TDD example

This repository illustrates, on the basis of a minimal example, what [Test-driven-development](https://en.wikipedia.org/wiki/Test-driven_development) could look like. The commit history (until c2dbaabea2a27df3804965dbf73e5e2becff240c) reveals the individual steps that were made during development.
Tests are written __before__ the actual code that makes them pass, but in order to arrive at a git history that only contains commits for which the tests pass, the commits contain both changes to the code and the tests.
For each commit, type checking and tests were executed with the following command (`pytest` and `mypy` are required dependencies):

```py
python -m pytest tests.py && mypy .
```


## Task description

The code solves a problem that could be stated as follows:

"Develop a _game_ class that allows adding and removing _players_, where each _player_ has a _name_ and a _health_ indicator (between 0 and 100; the initial _health_ is 100). A _player_'s _health_ must be adjustable to account for actions in the game. Moreover, there cannot be two players with the same name in one _game_, and when a player's health goes down to zero, the player becomes _inactive_. Finally, generalize the game class to operate on any _player_ type fulfilling a certain interface."
