from typing import Protocol


class Player(Protocol):
    @property
    def name(self) -> str:
        ...

    @property
    def health(self) -> int:
        ...


class Game:
    def __init__(self) -> None:
        self._players: list[Player] = []

    def add_player(self, player: Player) -> None:
        if self._get_player(player.name) is not None:
            raise ValueError(f"Player with name {player.name} already exists")
        self._players.append(player)

    def remove_player(self, name: str) -> Player:
        player = self.get_player(name)
        self._players.remove(player)
        return player

    def get_player(self, name: str) -> Player:
        player = self._get_player(name)
        if player is None:
            raise ValueError(f"No player with name {name} registered")
        return player

    def is_active_player(self, name: str) -> bool:
        return self.get_player(name).health > 0

    def _get_player(self, name: str) -> Player | None:
        for player in self._players:
            if name == player.name:
                return player
        return None
