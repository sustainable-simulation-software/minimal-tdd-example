class Player:
    def __init__(self, name: str) -> None:
        self._name = name
        self._health = 100

    @property
    def name(self) -> str:
        return self._name

    @property
    def health(self) -> int:
        return self._health

    def take_impact(self, strength: int) -> None:
        self._health -= strength
