from pytest import raises

from player import Player
from game import Game


def test_player_name():
    player = Player(name="Charlie")
    assert player.name == "Charlie"


def test_player_initial_health():
    player = Player(name="Charlie")
    assert player.health == 100


def test_player_health_reduction():
    player = Player(name="Charlie")
    player.take_impact(10)
    assert player.health == 90


def test_game_player_addition():
    game = Game()
    game.add_player(Player(name="Charlie"))


def test_game_player_retrieval():
    game = Game()
    game.add_player(Player(name="Charlie"))
    assert game.get_player(name="Charlie").name == "Charlie"


def test_game_player_removal():
    game = Game()
    game.add_player(Player(name="Charlie"))
    assert game.remove_player(name="Charlie").name == "Charlie"
    with raises(ValueError):
        game.get_player(name="Charlie")


def test_game_duplicate_player_insertion_fails():
    game = Game()
    game.add_player(Player(name="Charlie"))
    with raises(ValueError):
        game.add_player(Player(name="Charlie"))


def test_game_nonexisting_player_removal_fails():
    game = Game()
    game.add_player(Player(name="Charlie"))
    game.remove_player(name="Charlie")
    with raises(ValueError):
        game.remove_player(name="Charlie")


def test_game_zero_health_player_is_inactive():
    game = Game()
    game.add_player(Player(name="Charlie"))
    assert game.is_active_player(name="Charlie")
    game.get_player("Charlie").take_impact(100)
    assert not game.is_active_player(name="Charlie")
